package com.pajato.tks.search.movie.core

import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.pager.core.PageData
import com.pajato.tks.pager.core.PagedResultMovie

public interface SearchRepoMovie {
    public suspend fun getSearchPageMovie(key: SearchKey): PageData<PagedResultMovie>
    public suspend fun getSearchResults(key: SearchKey): List<PagedResultMovie>
}
